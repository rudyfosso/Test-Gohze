
le Mini projet contenu sur ce depot permet de simuler l'evolution de plusieurs tondeuses sur une pelouse ;

coté front end nous avons un formulaire prenant en parametres :
- les coordonnées du point le plus a droite en haut de la pelouse ecrit selon le meme formalisme que celui de l'exercice en python (ex: 5 5) 
- les paramtres de position de 3 tondeuses:qui est lui aussi une chaine de caractère selon le meme formalisme que dans l'exercice python  (Exemple: 1 2 N) separés par des espaces 

- les sequence de mouvement de chacune des tondeuses sous forme de chaine de caractère (Exemple: AGAGAGGA)
 

Pour cequi concerne la partie lancement de l'application cela se fait en deux phases :

- Front end 

Pour le front-end une fois que nous avons cloner le projet on va se placer dans le repertoire FRONT et ensuite  on va taper la commande :" npm install " pour installer les dependances necessaires au fonctionnement de l'application

il va falloir ensuite lancer le serveur du coté front-end cela se fait via la commande :npm start depuis le meme repertoir que precedemment;une fois cela fai on pourra aller au coté backend 

- Backend
pour cette partie on aura tout d'abord besoin de se placer dans le repertoire Gohze et en cas d'abscence de certaines packets nous allons les installer avec l'outil pip:

pip install django-cors-headers

pip install djangorestframework
pip install markdown       # Markdown support for the browsable API.
pip install django-filter  # Filtering support


Une fois que les packets manquants sont installés, on peux lancer le projet en utilisant la commande suivante dans le meme repertoire(Gohze) :
python manage.py runserver

si nous avons une indication de migration à la saisie de la commande precedente,nous pouvons corriger en faisaint :

python manage.py makemigrations

et ensuite 
python manage.py migrate puis on pourra relancer le serveur sans problème

NB: le repertoire d'execution des differentes commandes pour le backend est celui nommé Gohze et contanant à sa racine le fichier manage.py
pour le cas du Frontend il s'agit du repertoire Front contenant à sa racine le fichier de versionning des dependances package.json
